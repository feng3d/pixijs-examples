const app = new PIXI.Application({
    width: 800, height: 600, 
    // backgroundColor: 0x1099bb, resolution: window.devicePixelRatio || 1,
    // transparent: true,
    backgroundAlpha: 0
});
document.body.appendChild(app.view);

// create a new Sprite from an image path.
const bunny = PIXI.Sprite.from('examples/assets/bunny.png');

// center the sprite's anchor point
bunny.anchorX = 0.5;
bunny.anchorY = 0.5;

// move the sprite to the center of the screen
bunny.x = app.screen.width / 2;
bunny.y = app.screen.height / 2;

app.stage.addChild(bunny);

app.ticker.add(() =>
{
    // just for fun, let's rotate mr rabbit a little
    bunny.rotation += 0.1;
});
