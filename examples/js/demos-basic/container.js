const app = new PIXI.Application({
    width: 800, height: 600, backgroundColor: 0x1099bb, resolution: window.devicePixelRatio || 1,
});
document.body.appendChild(app.view);

const container = new PIXI.Container();

app.stage.addChild(container);

// Create a new texture
const texture = PIXI.Texture.from('examples/assets/bunny.png');

// Create a 5x5 grid of bunnies
for (let i = 0; i < 10000; i++)
{
    const bunny = new PIXI.Sprite();
    bunny.texture = texture;
    bunny.anchorX = 0.5;
    bunny.anchorY = 0.5;
    bunny.x = (i % 100) * 40;
    bunny.y = Math.floor(i / 100) * 40;
    container.addChild(bunny);
}

// Move container to the center
container.x = app.screen.width / 2;
container.y = app.screen.height / 2;

// Center bunny sprite in local container coordinates
container.pivotX = container.width / 2;
container.pivotY = container.height / 2;

container.scaleX = 0.15;
container.scaleY = 0.15;

// Listen for animate update
app.ticker.add((delta) =>
{
    // rotate the container!
    // use delta to create frame-independent transform
    container.rotation -= 0.01 * delta;
});
