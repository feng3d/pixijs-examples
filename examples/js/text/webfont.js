const app = new PIXI.Application({ backgroundColor: 0x1099bb });
document.body.appendChild(app.view);

// // Load them google fonts before starting...!
window.WebFontConfig = {
    google: {
        families: ['Snippet', 'Arvo:700italic', 'Podkova:700'],
    },

    active() {
        init();
    },
};

/* eslint-disable */
// include the web-font loader script
(function() {
    const wf = document.createElement('script');
    wf.src = `examples/assets/webfont/webfont.js`;
    wf.type = 'text/javascript';
    wf.async = 'true';
    const s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
}());
/* eslint-enabled */

function init() {
    // create some white text using the Snippet webfont
    const textSample = new PIXI.Text('Pixi.js text using the\ncustom "Snippet" Webfont', {
        fontFamily: 'Snippet',
        fontSize: 50,
        fill: 'white',
        align: 'left',
    });
    textSample.x = 50; textSample.y = 200;
    app.stage.addChild(textSample);
}
