const app = new PIXI.Application({ backgroundColor: 0x103322 });
document.body.appendChild(app.view);

// === FIRST PART ===
// just simple rotation

const sprite = new PIXI.projection.Sprite2d(PIXI.Texture.from('examples/assets/flowerTop.png'));
sprite.anchorX = 0.5;
sprite.anchorY = 1.0;
sprite.proj.affine = PIXI.projection.AFFINE.AXIS_X; // return to affine after rotating
sprite.x = app.screen.width * 1 / 8; sprite.y = app.screen.height / 2;
app.stage.addChild(sprite);

let step = 0;

app.ticker.add((delta) =>
{
    step += delta;
    sprite.rotation = step * 0.1;
});

// === SECOND PART ===
// lets also add scaling container

const scalingContainer = new PIXI.Container();
scalingContainer.scale.y = 0.3; // adjust scale by Y - that will change "perspective" a bit
scalingContainer.x = app.screen.width * 3 / 8; scalingContainer.y = app.screen.height / 2;
app.stage.addChild(scalingContainer);

const sprite2 = new PIXI.projection.Sprite2d(PIXI.Texture.from('examples/assets/flowerTop.png'));
sprite2.anchorX = 0.5;
sprite2.anchorY = 1.0;
sprite2.proj.affine = PIXI.projection.AFFINE.AXIS_X;
scalingContainer.addChild(sprite2);

app.ticker.add(() =>
{
    sprite2.rotation = step * 0.1;
});

// === THIRD PART ===
// Better isometry plane.
// We can even rotate it if you want!

const isoScalingContainer = new PIXI.Container();
isoScalingContainer.scale.y = 0.5; // isometry can be achieved by setting scaleY 0.5 or tan(30 degrees)
isoScalingContainer.x = app.screen.width * 6 / 8; isoScalingContainer.y = app.screen.height / 2;
app.stage.addChild(isoScalingContainer);

const isometryPlane = new PIXI.Graphics();
isometryPlane.rotation = Math.PI / 4;
isoScalingContainer.addChild(isometryPlane);

isometryPlane.lineStyle(2, 0xffffff);
for (let i = -100; i <= 100; i += 50)
{
    isometryPlane.moveTo(-150, i);
    isometryPlane.lineTo(150, i);
    isometryPlane.moveTo(i, -150);
    isometryPlane.lineTo(i, 150);
}

isometryPlane.drawCircle(0, 0, 100);

const sprite3 = new PIXI.projection.Sprite2d(PIXI.Texture.from('examples/assets/eggHead.png'));
sprite3.anchorX = 0.5;
sprite3.anchorY = 1.0;
sprite3.proj.affine = PIXI.projection.AFFINE.AXIS_X;
sprite3.scaleX = 0.3; sprite3.scaleY =  0.5; // make it small but tall!
// not-proportional scale can't work without special flag `scaleAfterAffine`
// fortunately, its `true` by default
isometryPlane.addChild(sprite3);

app.ticker.add(() =>
{
    sprite3.rotation = step * 0.05;
    const radius = 100; const
        speed = 0.005;
    sprite3.x = Math.cos(step * speed) * radius; sprite3.y = Math.sin(step * speed) * radius;
});
