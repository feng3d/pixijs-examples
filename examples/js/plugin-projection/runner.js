// this example uses both pixi-spine and pixi-projection
// it doesnt use projection-spine bridge because it uses only 2d version of spine object

const app = new PIXI.Application({ autoStart: false });
document.body.appendChild(app.view);

app.stop();

const { loader } = app;

// load spine data
loader
    .add('pixie', 'examples/assets/pixi-spine/pixie.json')
    .add('bg', 'examples/assets/pixi-spine/iP4_BGtile.jpg')
    .add('fg', 'examples/assets/pixi-spine/iP4_ground.png')
    .load(onAssetsLoaded);

const objs = []; let
    pixie;

app.stage.interactive = true;

// 1 earth and 2 parallax layers

const camera = new PIXI.projection.Camera3d();
camera.setPlanes(300, 10, 1000, false);
camera.x = app.screen.width / 2; camera.y = 0;
camera.position3d.y = -500; // camera is above the ground
app.stage.addChild(camera);

const groundLayer = new PIXI.projection.Container3d();
groundLayer.euler.x = Math.PI / 2;
camera.addChild(groundLayer);

// Those two layers can have 2d objects inside
// because they return everything to affine space

const bgLayer = new PIXI.projection.Container3d();
bgLayer.proj.affine = PIXI.projection.AFFINE.AXIS_X;
camera.addChild(bgLayer);
bgLayer.position3d.z = 80;

const mainLayer = new PIXI.projection.Container3d();
mainLayer.proj.affine = PIXI.projection.AFFINE.AXIS_X;
camera.addChild(mainLayer);

const repeats = 3;

function onAssetsLoaded(loaderInstance, res)
{
    for (let i = 0; i < repeats; i++)
    {
        // simple 2d sprite on back
        const bg = new PIXI.Sprite(res.bg.texture);
        bgLayer.addChild(bg);
        bg.position.x = bg.texture.width * i;
        bg.anchor.y = 1;
        objs.push(bg);
    }

    for (let i = 0; i < repeats; i++)
    {
        // 3d sprite on floor
        const fg = new PIXI.projection.Sprite3d(res.fg.texture);
        groundLayer.addChild(fg);
        fg.anchorX = 0;
        fg.anchorY = 0.5;
        // use position or position3d here, its not important,
        // unless you need Z - then you need position3d
        fg.position.x = fg.texture.width * i;
        objs.push(fg);
    }

    pixie = new PIXI.spine.Spine(res.pixie.spineData);
    pixie.x = 300; pixie.y = 0;
    pixie.scaleX = pixie.scaleY = 0.3;

    mainLayer.addChild(pixie);

    pixie.stateData.setMix('running', 'jump', 0.2);
    pixie.stateData.setMix('jump', 'running', 0.4);

    pixie.state.setAnimation(0, 'running', true);

    app.stage.on('pointerdown', onTouchStart);

    function onTouchStart()
    {
        pixie.state.setAnimation(0, 'jump', false);
        pixie.state.addAnimation(0, 'running', true, 0);
    }

    app.start();
}

app.ticker.add((delta) =>
{
    pixie.position.x += 10 * delta;

    // camera looks on pixi!
    camera.position3d.x = pixie.position.x;

    objs.forEach((obj) =>
    {
        if (obj.position.x + obj.texture.width < pixie.position.x - 500)
        {
            obj.position.x += repeats * obj.texture.width;
        }
    });
});
